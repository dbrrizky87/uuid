<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\support\Str;
use App\Traits\UuidTrait;

class Otp extends Model
{
    use UuidTrait;

    protected $table = "otp_codes";

    protected static function boot(){
        parent::boot();
        UuidTrait::bootUuidTrait();
    }

    /**
     * Get the value indicating whether the IDs are incrementing.
     *
     * @return bool
     */
        
    public function getIncrementing(){
        return false;
    }


    /**
    * Get the auto-incrementing key type.
    *
    * @return string
    */
       
   public function getKeyType(){
       return "string";
   }




}
