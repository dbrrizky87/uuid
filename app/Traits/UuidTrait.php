<?php

namespace App\Traits;

use Illuminate\support\Str;

trait UuidTrait {
    

    public static function bootUuidTrait(){
        
        $role = new App\Role;
        static::creating(function ($model){
            if (! $model->getKey()) {
                $model->{$model->getKeyName()} = (string) Str::uuid();
            }
        });
    }
}
